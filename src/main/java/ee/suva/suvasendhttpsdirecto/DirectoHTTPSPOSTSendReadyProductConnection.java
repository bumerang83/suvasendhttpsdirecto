// Gets connection to send information about ready Sockmann products (Mare Lee was sending infornation to MSMicro/Store)

package ee.suva.suvasendhttpsdirecto;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * Author: bumerrrang
 * 
 * File:   DirectoHTTPSPOSTSendReadyProductConnection.java 
 *
 * Created on: 06-Dec-2016, 19:36:49
 *
 * Project name: SUVAHTTPSProject
 *
 */
public class DirectoHTTPSPOSTSendReadyProductConnection extends AbstractConnection {
    private HttpsURLConnection con;  // the being used HTTPS POST connection to Directo
    private URL url;  // the URL the system connects with
        
    @Override
    public void setConnection(String url) {
        try {
//            this.setUrl(new URL("https://directo.gate.ee/xmlcore/sockmann_group/xmlcore.asp"));
            this.setUrl(new URL(url));
            this.setUrl(this.getUrl());
            con = (HttpsURLConnection)this.getUrl().openConnection();
        } catch (IOException ex) {
            Logger.getLogger(DirectoHTTPSPOSTSendReadyProductConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the con
     */
    // this method starts the connection
    public HttpsURLConnection getCon() {  // return the connection
        if(con!=null) {
            return con;
        }
        else {
            return null;
        }
    }
    
    // this method closes the connection
    public void closeCon() {
        if(con!=null) {
            con.disconnect();
        }
    }

    /**
     * @return the url
     */
    @Override
    public URL getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    @Override
    public void setUrl(URL url) {
        this.url = url;
    }
}
