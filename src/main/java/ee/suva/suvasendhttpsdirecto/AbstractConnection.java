// Abstract class to be the general for all connections

package ee.suva.suvasendhttpsdirecto;

import java.net.URL;

/**
 *
 * Author: bumerrrang
 
 File:   AbstractConnection.java 

 Created on: 06-Dec-2016, 19:29:03

 Project name: SUVAHTTPSProject
 *
 */
public abstract class AbstractConnection {
    
    private URL url;  // the url application connects to
    
    abstract public void setConnection(String url);  // sets the required connection be defined url

    /**
     * @return the url
     */
    public URL getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(URL url) {
        this.url = url;
    }
}
