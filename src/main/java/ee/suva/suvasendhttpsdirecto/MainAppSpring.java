/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ee.suva.suvasendhttpsdirecto;

import java.io.IOException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
 * @author    : bumerang    
 * Document   : MainAppSpring
 * Created on : Mar 13, 2017, 2:36:31 PM
 */
public class MainAppSpring {
   // use: Beans.xml https://directo.gate.ee/xmlcore/sockmann_group/xmlcore.asp Directo_POST_ready_product/ archive/
   // in command prompt for Directo project
   public static void main(String[] args) throws IOException {
      // only if command prompt arguments were defined
      // args[0] = Beans.xml, the Spring file with configs
      // args[1] = https://directo.gate.ee/xmlcore/sockmann_group/xmlcore.asp an URL for sending data
      // args[2] = Directo_POST_ready_product/ a folder the FTP server gets data into (source for sending data to Directo)
      // args[3] = archive/ a folder for saving the been sent files
      if(args.length==4) {
          // first command prompt argument below, destination of the main configuration Spring file (Beans.xml in this case)
          ApplicationContext context =
                new ClassPathXmlApplicationContext(args[0]);

            POSTReadyProduct prp = (POSTReadyProduct) context.getBean("postreadyproduct");
            prp.sendReadyProductInformation(args[2], args[3], args[1]);
      }
      else {
          System.out.println("Wrong format of calling the application!\nThere must be five arguments in the prompt.");
      }
    }
}