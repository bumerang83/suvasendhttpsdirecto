// the class will send ready product information to Directo

package ee.suva.suvasendhttpsdirecto;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ProtocolException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * Author: bumerrrang
 * 
 * File:   POSTReadyProduct.java 
 *
 * Created on: 06-Dec-2016, 20:06:26
 *
 * Project name: SUVAHTTPSProject
 *
 */
public class POSTReadyProduct {
    final static org.apache.log4j.Logger LOGGER_SYSTEM = org.apache.log4j.Logger.getLogger(MainApp.class);  // log for system records
    
    /**
     *
     * @param directory_from_ftp
     * @param directory_archive
     * @param url
     * @throws ProtocolException
     * @throws IOException
     */
    // checks all the directed directory_from_ftp, move archives to the defined archive directory
    public void sendReadyProductInformation(String directory_from_ftp, String directory_archive, String url) throws ProtocolException, IOException {
        LOGGER_SYSTEM.info("The sending ready products from Sockmann to Directo by HTTPS POST logs\n\n");  // log for system records
        File[] fileList;
        File dir = new File(directory_from_ftp);  // gets the defined as the argument directory_from_ftp
        
        // works online
        while(true) {
            // makes a brake to release resources
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                LOGGER_SYSTEM.error("AN ERROR OCCURED WHILE SLEEPING PROCESS!\n\n");
            }

            if(dir.isDirectory()) {
                fileList = dir.listFiles();

                for(File file: fileList) {
                    DirectoHTTPSPOSTSendReadyProductConnection directoConnection = new DirectoHTTPSPOSTSendReadyProductConnection();
                    sendRequest(file, directoConnection, url);
                  //  DirectoHTTPSPOSTSendReadyProductConnection directoConnection = new DirectoHTTPSPOSTSendReadyProductConnection();
                  //  directoConnection.setConnection(url);

                    // defines the request required headers
                  //  directoConnection.getCon().setRequestMethod("POST");
                  //  directoConnection.getCon().setRequestProperty("Content-Type","application/x-www-form-urlencoded");

                    // defines the request required parameters
                  //  StringBuilder urlParameters = new StringBuilder("put=1&what=incoming&xmldata=");

                    // enables the output
                  //  directoConnection.getCon().setDoOutput(true);
                    // gets stream for writing request
                  //  try (DataOutputStream writer = new DataOutputStream(directoConnection.getCon().getOutputStream())) {
                        // Open input file to send to Directo
                  //      FileInputStream fstream = new FileInputStream(file);
                  //      try (BufferedReader br = new BufferedReader(new InputStreamReader(fstream))) {
                  //          int strNum = 0;  // the been read string number
                  //          String strLine;
                            //Read File Line By Line
                  //          while((strLine = br.readLine())!=null) {
                  //              if(strNum!=0) {  // misses the first read string
                  //                  urlParameters.append(strLine.trim());
                  //              }
                  //              strNum++;  // one more line was read
                  //          }
                  //          LOGGER_SYSTEM.info("\nPROCESSING FILE: " + file.getName() + "\nTHE REQUEST : " + urlParameters);
                  //          writer.writeBytes(urlParameters + "\n\n");

                  //      }  // explicitly closes the input stream
                  //      writer.flush();   // send all immediately

                        // Moves files to the archive folder
                    moveFile(file, directory_archive);
                        // creates the timestamp
                      //  DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
                      //  Date date = new Date();

                      //  File writeTo = new File(directory_archive + file.getName() + "_" + dateFormat.format(date));
                      //  writeTo.createNewFile();
                      //  file.renameTo(writeTo);
                  //  }  // closes the server output stream automatically with try{}

                    // receives response
                    receiveResponse(directoConnection);
                  //  int i = directoConnection.getCon().getResponseCode();
                    // reads the server response
                  //  try ( DataInputStream input = new DataInputStream(directoConnection.getCon().getInputStream())) {
                  //          StringBuilder bufread = new StringBuilder();
                  //          for (int c = input.read(); c != -1; c = input.read()) {
                  //              bufread.append((char)c);
                  //          }
                  //          LOGGER_SYSTEM.info("\nTHE RESPONSE : " + bufread);
                  //  }  // closes the server response stream in the end automatically by try {} block
                  //  directoConnection.getCon().disconnect();
                }
            }
        }
    }

    // sends request to defined url
    public void sendRequest(File file, DirectoHTTPSPOSTSendReadyProductConnection directoConnection, String url) {
        //DirectoHTTPSPOSTSendReadyProductConnection directoConnection = new DirectoHTTPSPOSTSendReadyProductConnection();
        directoConnection.setConnection(url);

        // defines the request required headers
        try {
            directoConnection.getCon().setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        directoConnection.getCon().setRequestProperty("Content-Type","application/x-www-form-urlencoded");

        // defines the request required parameters
        StringBuilder urlParameters = new StringBuilder("put=1&what=incoming&xmldata=");

        // enables the output
        directoConnection.getCon().setDoOutput(true);
        // gets stream for writing request
        try (DataOutputStream writer = new DataOutputStream(directoConnection.getCon().getOutputStream())) {
            // Open input file to send to Directo
            FileInputStream fstream = new FileInputStream(file);
            try (BufferedReader br = new BufferedReader(new InputStreamReader(fstream))) {
                int strNum = 0;  // the been read string number
                String strLine;
                //Read File Line By Line
                while((strLine = br.readLine())!=null) {
                    if(strNum!=0) {  // misses the first read string
                        urlParameters.append(strLine.trim());
                    }
                    strNum++;  // one more line was read
                }
                LOGGER_SYSTEM.info("\nPROCESSING FILE: " + file.getName() + "\nTHE REQUEST : " + urlParameters);
                writer.writeBytes(urlParameters + "\n\n");

            }  // explicitly closes the input stream
            writer.flush();   // send all immediately

        }  // closes the server output stream automatically with try{}
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    // receives response from url
    public void receiveResponse(DirectoHTTPSPOSTSendReadyProductConnection directoConnection) {
        try {
            int i = directoConnection.getCon().getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // reads the server response
        try ( DataInputStream input = new DataInputStream(directoConnection.getCon().getInputStream())) {
            StringBuilder bufread = new StringBuilder();
            for (int c = input.read(); c != -1; c = input.read()) {
                bufread.append((char)c);
            }
            LOGGER_SYSTEM.info("\nTHE RESPONSE : " + bufread + "\n");
        }  // closes the server response stream in the end automatically by try {} block
        catch (IOException e) {
            e.printStackTrace();
        }
        directoConnection.getCon().disconnect();
    }

    // moves file to archive folder
    public void moveFile(File file, String directory_archive) {
        // Moves files to the archive folder
        // creates the timestamp
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date date = new Date();

        File writeTo = new File(directory_archive + file.getName() + "_" + dateFormat.format(date));
        try {
            writeTo.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        file.renameTo(writeTo);
    }
}