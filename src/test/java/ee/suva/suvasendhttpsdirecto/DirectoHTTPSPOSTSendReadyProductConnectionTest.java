/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.suva.suvasendhttpsdirecto;

import java.net.MalformedURLException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bumerang
 */
public class DirectoHTTPSPOSTSendReadyProductConnectionTest {
    
    public DirectoHTTPSPOSTSendReadyProductConnectionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setConnection method, of class DirectoHTTPSPOSTSendReadyProductConnection.
     */
    @Test
    public void testSetConnection() {  // checks if connection was established
        System.out.println("setConnection");
//        String url = "";
        String url = "https://directo.gate.ee/xmlcore/sockmann_group/xmlcore.asp";
//        DirectoHTTPSPOSTSendReadyProductConnection instance = new DirectoHTTPSPOSTSendReadyProductConnection();
        DirectoHTTPSPOSTSendReadyProductConnection instance = new DirectoHTTPSPOSTSendReadyProductConnection();
//        instance.setConnection(url);
        instance.setConnection(url);
        assertNotEquals(instance.getCon(), null);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of getCon method, of class DirectoHTTPSPOSTSendReadyProductConnection.
     */
    @Test
    public void testGetCon() {  // checks if object has a valid connection
        System.out.println("getCon");
        String url = "https://directo.gate.ee/xmlcore/sockmann_group/xmlcore.asp";
        DirectoHTTPSPOSTSendReadyProductConnection instance = new DirectoHTTPSPOSTSendReadyProductConnection();
        HttpsURLConnection expResult = null;
        instance.setConnection(url);
        HttpsURLConnection result = instance.getCon();
        assertNotEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of closeCon method, of class DirectoHTTPSPOSTSendReadyProductConnection.
     */
    @Test
    public void testCloseCon() {  // checks if connection was completely closed
        System.out.println("closeCon");
        DirectoHTTPSPOSTSendReadyProductConnection instance = new DirectoHTTPSPOSTSendReadyProductConnection();
        instance.closeCon();
        assertNotEquals(instance, null);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of getUrl method, of class DirectoHTTPSPOSTSendReadyProductConnection.
     * @throws java.net.MalformedURLException
     */
    @Test
    public void testGetUrl() throws MalformedURLException { // checks if returns well formed URL, it means created url exists
        System.out.println("getUrl");
        DirectoHTTPSPOSTSendReadyProductConnection instance = new DirectoHTTPSPOSTSendReadyProductConnection();
        instance.setUrl(new URL("https://directo.gate.ee/xmlcore/sockmann_group/xmlcore.asp"));
//        this.setUrl(new URL(url));
        URL expResult = null;
        URL result = instance.getUrl();
        assertNotEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of setUrl method, of class DirectoHTTPSPOSTSendReadyProductConnection.
     * @throws java.net.MalformedURLException
     */
    @Test
    public void testSetUrl() throws MalformedURLException {  // checks if there was created a proper connection
        System.out.println("setUrl");
//        URL url = null;
        URL url = new URL("https://directo.gate.ee/xmlcore/sockmann_group/xmlcore.asp");
        DirectoHTTPSPOSTSendReadyProductConnection instance = new DirectoHTTPSPOSTSendReadyProductConnection();
        instance.setUrl(url);
        assertNotEquals(instance.getUrl(), null);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }


}
