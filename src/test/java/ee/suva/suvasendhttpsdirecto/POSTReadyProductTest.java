/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.suva.suvasendhttpsdirecto;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 *
 * @author bumerang
 */
public class POSTReadyProductTest {
    
    public POSTReadyProductTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of sendReadyProductInformation method, of class POSTReadyProduct.
     */
    @Test
    public void testSendReadyProductInformation() throws Exception {
        System.out.println("sendReadyProductInformation");
//        String directory_from_ftp = "";
        String directory_from_ftp = "Directo_POST_ready_product/";
//        String directory_archive = "";
        String directory_archive = "archive/";
//        String url = "";
        String url = "https://directo.gate.ee/xmlcore/sockmann_group/xmlcore.asp";
//        String logs = "";
        String logs = "logs.txt";
        POSTReadyProduct instance = new POSTReadyProduct();
        assertNotEquals(instance, null);
        // endless loop below, does not matter to start it
//        instance.sendReadyProductInformation(directory_from_ftp, directory_archive, url, logs);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of sendRequest method, of class POSTReadyProduct.
     */
    @Test
    // sends request to defined url
    public void testSendRequest() {
        //DirectoHTTPSPOSTSendReadyProductConnection directoConnection = new DirectoHTTPSPOSTSendReadyProductConnection();
        POSTReadyProduct prp = new POSTReadyProduct();
        prp.sendRequest(new File("32840_test.xml"), new DirectoHTTPSPOSTSendReadyProductConnection(), "https://directo.gate.ee/xmlcore/sockmann_group/xmlcore.asp");
    }

    /**
     * Test of receiveResponse method, of class POSTReadyProduct.
     */
    @Test
    // receive response from defined url
    public void testReceiveResponse() {
        DirectoHTTPSPOSTSendReadyProductConnection directoConnection = new DirectoHTTPSPOSTSendReadyProductConnection();
        POSTReadyProduct prp = new POSTReadyProduct();
        prp.sendRequest(new File("32840_test.xml"), directoConnection, "https://directo.gate.ee/xmlcore/sockmann_group/xmlcore.asp");
        prp.receiveResponse(directoConnection);
    }

    /**
     * Test of moveFile method, of class POSTReadyProduct.
     */
    @Test
    // moving, etc renaming fileprp.receiveResponse(directoConnection);
    public void testMoveFile() throws InterruptedException {
        Thread.sleep(3000);
        POSTReadyProduct prp = new POSTReadyProduct();
        prp.moveFile(new File("32840_test.xml"), "test/");
    }
}